# Assignment 6

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a06`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*40%*)

Write a program `p1.py` that converts the string `'Quintessence'` to a list using the function `list()`
and then performs the following tasks (the results should be printed)

1. Count the number of items in the list
1. Delete all occurrences of the item 's' in the list
1. Delete all occurrences of the item 'e' in the list




### Problem 2 (*60%*)

Write a function `shuffle()` that takes a list as a parameter and returns a new list with the elements
shuffled into a random order. The following code fragment seems to work with an example list.
However — it is not a function yet, and it has a flaw. Namely, what happens to the old list after all the work?

1. Fix the flaw
1. Put the correct code into a function as described above
1. Make sure to test your function for two different lists of different lengths in the program

```python
oldlist = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

l = len(oldlist)
newlist = []
for i in range(l):
	randchar = oldlist[random.randrange(l)]
	oldlist.remove(randchar)
	l = l - 1
	newlist.append(randchar)

print(newlist)
```
